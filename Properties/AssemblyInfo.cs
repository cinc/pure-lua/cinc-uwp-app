﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("CinC.App")]
[assembly: AssemblyDescription("UWP Client for the Commander in Chief 3rd party plugin for Digital Combat Simulator")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Jeffrey Jones")]
[assembly: AssemblyProduct("CinC.App")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.0.1.0")]
[assembly: AssemblyFileVersion("0.0.1.0")]
[assembly: ComVisible(false)]
[assembly: NeutralResourcesLanguage("")]

