﻿# Commander In Chief Client Application

## Introduction

An Application that aims to allow for "Game Master" level control of an active
Digital Combat Simulator mission. For use in multiplayer sessions to make
players lives hell, as all Game Masters should.

Written in UWP (Universal Windows Platform) which, despite what the name
implies, only runs on Windows 10 and Windows 10 based devices.

First even UWP application written by RurouniJones so be warned.

## Current Status

Hilariously experimental and not fit for human use right now.

![Screenshot showing current status](screenshot.png)

## For Developers

MIT License

Source Code available at https://gitlab.com/cinc/cinc-uwp-app
