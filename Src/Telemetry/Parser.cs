﻿// Copyright 2018 Jeffrey Jones
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

namespace CinC.App.Telemetry
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using CinC.App.Telemetry.Events;

    internal class Parser
    {
        private readonly string fileType;
        private readonly string fileVersion;
        private readonly StreamReader streamReader;
        private decimal referenceLatitude = 0;
        private decimal referenceLongitude = 0;

        public Parser(StreamReader streamReader)
        {
            this.streamReader = streamReader;
            this.fileType = streamReader.ReadLine();
            this.fileVersion = streamReader.ReadLine();
        }

        public Event ParseEvent()
        {
            string eventString = this.streamReader.ReadLine();
            if (eventString.Substring(0, 2) == "0,")
            {
                string[] fields = eventString.Substring(2).Split("=");
                string key = fields[0];
                string value = fields[1];

                if (key == "Comments" || key == "Briefing")
                {
                    return this.ParseMultiline(key, value);
                }
                else if (key == "ReferenceLatitude")
                {
                    this.referenceLatitude = decimal.Parse(value);
                    return new GlobalProperty(key, value);
                }
                else if (key == "ReferenceLongitude")
                {
                    this.referenceLongitude = decimal.Parse(value);
                    return new GlobalProperty(key, value);
                }
                else
                {
                    return new GlobalProperty(fields[0], fields[1]);
                }
            }
            else if (eventString[0] == '#')
            {
                return new GlobalProperty("TimeUpdate", eventString.Substring(1));
            }
            else if (eventString[0] == '-')
            {
                return new ObjectDeletion(eventString.Substring(1));
            }
            else
            {
                return this.ParseObjectUpdate(eventString);
            }
        }

        private Event ParseMultiline(string key, string firstLine)
        {
            string commentLine;

            if (firstLine.Substring(firstLine.Length - 1) != "\\")
            {
                return new GlobalProperty(key, firstLine);
            }
            else
            {
                StringBuilder fullComment = new StringBuilder(firstLine.Substring(0, firstLine.Length - 1));
                while ((commentLine = this.streamReader.ReadLine()).Substring(commentLine.Length - 1) == "\\")
                {
                    fullComment.AppendLine(commentLine.Substring(0, commentLine.Length - 1));
                }

                return new GlobalProperty(key, fullComment.ToString());
            }
        }

        // 510,T=5.2230023|4.8425185|638.44|13.3|-2.6|246.1|572606.75|-221873.55|240.5,Type=Air+FixedWing,Name=FA-18C_hornet,Pilot=KILLER | GRYPHON 211,Group=**5) [CAP]  F/A-18C (Sukhumi) (Ground) #003,Color=Blue,Coalition=Enemies,Country=us
        private Event ParseObjectUpdate(string objectLine)
        {
            List<string> fields = new List<string>(objectLine.Split(","));

            var objectUpdate = new ObjectUpdate();
            objectUpdate.Id = fields[0];
            fields.RemoveAt(0);

            foreach (string field in fields)
            {
                string key = field.Split("=")[0].ToLower();
                string value = field.Split("=")[1];

                switch (key)
                {
                    case "t":
                        this.ParseLocation(objectUpdate, value);
                        break;
                    case "name":
                        objectUpdate.Name = value;
                        break;
                    case "type":
                        objectUpdate.Type = value;
                        break;
                    case "coalition":
                        objectUpdate.Coalition = value;
                        break;
                    case "country":
                        objectUpdate.Country = value;
                        break;
                    case "group":
                        objectUpdate.Group = value;
                        break;
                    case "color":
                        objectUpdate.Color = value;
                        break;
                    case "pilot":
                        objectUpdate.Pilot = value;
                        break;
                }
            }

            return objectUpdate;
        }

        private void ParseLocation(ObjectUpdate objectUpdate, string locationData)
        {
            var values = locationData.Split("|");
            objectUpdate.Longitude = decimal.TryParse(values[0], out decimal longitude) ? this.referenceLongitude + longitude : (decimal?)null;
            objectUpdate.Latitude = decimal.TryParse(values[1], out decimal latitude) ? this.referenceLatitude + latitude : (decimal?)null;
            objectUpdate.Altitude = decimal.TryParse(values[2], out decimal altitude) ? altitude : (decimal?)null;

            if (values.Length == 9)
            {
                objectUpdate.Heading = decimal.TryParse(values[8], out decimal heading) ? heading : (decimal?)null;
            }
        }
    }
}
