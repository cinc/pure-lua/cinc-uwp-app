﻿// Copyright 2018 Jeffrey Jones
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

namespace CinC.App.Telemetry
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Threading.Tasks;
    using CinC.App.Telemetry.Events;
    using Windows.Networking.Sockets;

    public class Client
    {
        private StreamSocket streamSocket;
        private Parser parser;

        public Client()
        {
            this.streamSocket = new StreamSocket();
        }

        public async Task<bool> ConnectAsync(string host, string port)
        {
            var hostName = new Windows.Networking.HostName(host);

            await this.streamSocket.ConnectAsync(hostName, port);
            Stream inputStream = this.streamSocket.InputStream.AsStreamForRead();
            Stream outputStream = this.streamSocket.OutputStream.AsStreamForWrite();

            var streamReader = new StreamReader(inputStream);
            var streamWriter = new StreamWriter(outputStream);

            var stream = streamReader.ReadLine();
            var format = streamReader.ReadLine();
            var serverRecorder = streamReader.ReadLine();
            streamReader.Read(); // Read the solo carriage return.

            Debug.WriteLine("Incoming Telemetry Handshake");
            Debug.WriteLine(stream);
            Debug.WriteLine(format);
            Debug.WriteLine(serverRecorder);

            string clientHandshake = "XtraLib.Stream.0\nTacview.RealTimeTelemetry.0\nCinC\n0\0";
            streamWriter.Write(clientHandshake);
            streamWriter.Flush();
            Debug.WriteLine(clientHandshake);
            Debug.WriteLine(string.Empty); // Empty line after sending the \0 terminated string
            Debug.WriteLine("Outgoing Telemetry Handshake");

            this.parser = new Parser(streamReader);
            return true;
        }

        public Event GetEvent()
        {
            return this.parser.ParseEvent();
        }
    }
}
