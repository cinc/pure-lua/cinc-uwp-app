﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;

namespace CinC.App.Telemetry
{
    class MapObject
    {
        public string Id { get; set; }
        public Geopoint Location { get; set; }
        public string Title { get; set; }
    }
}
