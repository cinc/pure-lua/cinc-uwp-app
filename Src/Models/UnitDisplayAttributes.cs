﻿namespace CinC.App.Models
{
    class UnitDisplayAttributes
    {
        public string DisplayName { get; set; }

        public string IconName { get; set; }
    }
}
