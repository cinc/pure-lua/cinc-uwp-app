﻿// Copyright 2018 Jeffrey Jones
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

namespace CinC.App.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CinC.App.Models;
    using CinC.App.Telemetry.Events;
    using Newtonsoft.Json;
    using Windows.ApplicationModel.Core;
    using Windows.Devices.Geolocation;
    using Windows.Storage;
    using Windows.UI.Core;
    using Windows.UI.Xaml;

    public class OrderOfBattleViewModel : NotificationBase
    {
        private Dictionary<string, MapUnitViewModel> orderOfBattle;
        private CoreDispatcher dispatcher = (Window.Current == null) ? CoreApplication.MainView.CoreWindow.Dispatcher : CoreApplication.GetCurrentView().CoreWindow.Dispatcher;
        private Dictionary<string, UnitDisplayAttributes> unitDisplayMapping;

        public OrderOfBattleViewModel()
        {
            this.orderOfBattle = new Dictionary<string, MapUnitViewModel>();
            this.PopulatUnitMappings();
        }

        public ObservableCollection<MapUnitViewModel> OrderOfBattle { get; set; } = new ObservableCollection<MapUnitViewModel>();

        public async Task<bool> WriteMapUnit(Event telemetryEvent)
        {
            if (telemetryEvent is ObjectUpdate updateEvent)
            {
                if (this.orderOfBattle.Keys.Any(key => key == updateEvent.Id))
                {
                    await this.UpdateMapUnit(this.orderOfBattle[updateEvent.Id], updateEvent);
                }
                else
                {
                    await this.AddMapUnit(updateEvent);
                }
            }
            else if (telemetryEvent is ObjectDeletion deletionEvent)
            {
                await this.DeleteMapUnit(this.orderOfBattle[deletionEvent.Id]);
            }

            return true;
        }

        public async Task<bool> AddMapUnit(ObjectUpdate telemetryEvent)
        {
            var location = new Geopoint(new BasicGeoposition() { Latitude = (double)telemetryEvent.Latitude, Longitude = (double)telemetryEvent.Longitude });

            var mapUnit = new MapUnitViewModel
            {
                Id = telemetryEvent.Id,
                Location = location,
                Pilot = telemetryEvent.Pilot,
                Type = telemetryEvent.Type,
                Coalition = telemetryEvent.Coalition,
                Country = telemetryEvent.Country,
                Color = telemetryEvent.Color,
                Icon = this.SetIcon(telemetryEvent.Color, telemetryEvent.Type, telemetryEvent.Name)
            };

            if (telemetryEvent.Altitude != null)
            {
                mapUnit.Altitude = Math.Round((decimal)telemetryEvent.Altitude * 3 / 100, 0) * 100;  
            }

            if ( telemetryEvent.Name != null && unitDisplayMapping.TryGetValue(telemetryEvent.Name, out UnitDisplayAttributes result)) {
                mapUnit.Name = result.DisplayName;
            }
            else
            {
                mapUnit.Name = telemetryEvent.Name;
            }

            this.orderOfBattle[telemetryEvent.Id] = mapUnit;

            await this.dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                OrderOfBattle.Add(orderOfBattle[telemetryEvent.Id]);
            });
            return true;
        }

        private string SetIcon(string color, string type, string name)
        {
            StringBuilder imagePath = new StringBuilder("ms-appx:///Assets/MapSymbols/");
            imagePath.Append(color);

            if (name != null && unitDisplayMapping.TryGetValue(name, out UnitDisplayAttributes result))
            {
                return imagePath.Append(result.IconName).Append(".png").ToString();
            }
            else
            {
                return "ms-appx:///Assets/MapSymbols/Unknown.png";
            }
        }

        private async Task<bool> UpdateMapUnit(MapUnitViewModel mapUnit, ObjectUpdate telemetryEvent)
        {
            Geopoint objectPoint;
            var latitude = telemetryEvent.Latitude == null ? mapUnit.Location.Position.Latitude : (double)telemetryEvent.Latitude;
            var longitude = telemetryEvent.Longitude == null ? mapUnit.Location.Position.Longitude : (double)telemetryEvent.Longitude;
            objectPoint = new Geopoint(new BasicGeoposition() { Latitude = latitude, Longitude = longitude });
            await this.dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                mapUnit.Location = objectPoint;
                if (telemetryEvent.Altitude != null)
                {
                    mapUnit.Altitude = Math.Round((decimal)telemetryEvent.Altitude * 3 / 100, 0) * 100;
                }
            });
            return true;
        }

        private async Task<bool> DeleteMapUnit(MapUnitViewModel mapUnit)
        {
            await this.dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                OrderOfBattle.Remove(mapUnit);
                orderOfBattle.Remove(mapUnit.Id);
            });

            return true;
        }

        private async Task<bool> PopulatUnitMappings()
        {

            Uri appUri = new Uri("ms-appx:///Assets/UnitMappings.json");
            StorageFile file = StorageFile.GetFileFromApplicationUriAsync(appUri).AsTask().ConfigureAwait(false).GetAwaiter().GetResult();

            unitDisplayMapping = JsonConvert.DeserializeObject<Dictionary<string, UnitDisplayAttributes>>(await FileIO.ReadTextAsync(file));

            return true;
        }
    }
}
