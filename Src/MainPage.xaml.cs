﻿// Copyright 2018 Jeffrey Jones
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//
// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace CinC.App
{
    using System;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using CinC.App.Dialog;
    using CinC.App.Telemetry;
    using CinC.App.Telemetry.Events;
    using CinC.App.ViewModels;
    using Windows.Devices.Geolocation;
    using Windows.Storage;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Controls.Maps;

    /// <summary>
    /// Provides implementation for for the Main Page.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private Client telemetryClient;
        private OrderOfBattleViewModel orderOfBattleViewModel = new OrderOfBattleViewModel();

        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Runs all initialization code to setup the map for the Caucuses.
        /// At some point this will need to be smarter and setup the map based on what the server is running but this is good enough for initial testing.
        /// We will use a MapSettings.json file to contain all the customizations we want to make to the map. For the moment the settings are mainly to do with removing unneeded clutter.
        /// </summary>
        /// <param name="sender">
        /// Object this event handler is attached to.
        /// See https://docs.microsoft.com/en-us/windows/uwp/xaml-platform/events-and-routed-events-overview#the-sender-parameter-and-event-data for more information.
        /// </param>
        /// <param name="e">
        /// Event data passed into this event handler.
        /// See https://docs.microsoft.com/en-us/windows/uwp/xaml-platform/events-and-routed-events-overview#the-sender-parameter-and-event-data for more information.
        /// </param>
        private void MainMap_Loaded(object sender, RoutedEventArgs e)
        {
            /* Setting API key crashes program  in similar fashion to the link below so we will leave it for the moment.
             * https://social.msdn.microsoft.com/Forums/en-US/91336963-b205-4074-ad36-192ff045e44d/trying-to-set-mapservicetoken-on-insider-build-causes-a-crash?forum=bingmapswindows8
             * MainMap.MapServiceToken = "API-KEY-HERE";
             */

            BasicGeoposition mapCenter = new BasicGeoposition();
            mapCenter.Latitude = 43;
            mapCenter.Longitude = 42;
            MainMap.Center = new Geopoint(mapCenter);
            MainMap.ZoomLevel = 8;

            Uri appUri = new Uri("ms-appx:///Assets/MapSettings.json");
            StorageFile file = StorageFile.GetFileFromApplicationUriAsync(appUri).AsTask().ConfigureAwait(false).GetAwaiter().GetResult();
            MainMap.StyleSheet = MapStyleSheet.ParseFromJson(FileIO.ReadTextAsync(file).AsTask().ConfigureAwait(false).GetAwaiter().GetResult());
        }

        private async void AppBarConnectButton_Click(object sender, RoutedEventArgs e)
        {
            ConnectionDialog connectionDialog = new ConnectionDialog();
            await connectionDialog.ShowAsync();

            if (connectionDialog.Result == ConnectionResult.Connected)
            {
                ConnectButton.Visibility = Visibility.Collapsed;
                DisconnectButton.Visibility = Visibility.Visible;
                this.telemetryClient = connectionDialog.TelemetryClient;
                await Task.Run(async () =>
                {
                    Event telemetryEvent;
                    while ((telemetryEvent = telemetryClient.GetEvent()) != null)
                    {
                        await orderOfBattleViewModel.WriteMapUnit(telemetryEvent);
                    }
                });
            }
        }

        private void AppBarDisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            DisconnectButton.Visibility = Visibility.Collapsed;
            ConnectButton.Visibility = Visibility.Visible;
        }
    }
}
